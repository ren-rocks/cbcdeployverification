```
Usage: cbcdeployverification [options...] file

    verifies that script is ready for deployment
    -h  show this message

```

Got tired of forgetting little things like shebang and decided to make a script to check multiple files for issues. Will update as I work on new scripts.
