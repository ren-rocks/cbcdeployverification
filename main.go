package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"

	"github.com/pkg/errors"
)

var (
	args  []string
	usage = `Usage: cbcdeployverification [options...] file

	verifies that script is ready for deployment
	-h	show this message
`
)

func main() {
	var err error
	defer func() {
		if err != nil {
			println(errors.Wrap(err, "exiting"))
			os.Exit(1)
		}
	}()

	flag.Usage = func() {
		fmt.Fprint(os.Stderr, fmt.Sprint(usage))
	}
	flag.Parse()

	args = flag.Args()
	if len(args) <= 0 {
		flag.Usage()
		os.Exit(1)
	}

	if err = hasShebang(); err != nil {
		return
	}

	fmt.Printf("file is ready to deploy\n")
	os.Exit(0)
}

func hasShebang() error {
	dat, err := ioutil.ReadFile(args[0])
	if err != nil {
		return err
	}

	var re = regexp.MustCompile(`(?m)\#\!\/usr\/bin\/[a-zA-Z]*\s?php`)
	if !re.Match(dat) {
		return errors.New("file does not contain shebang")
	}

	return nil
}
